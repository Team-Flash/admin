var dayscount = [{
    "country": "0",
    "visits": 0
}, {
    "country": "1",
    "visits": 0
}, {
    "country": "2",
    "visits": 0
}, {
    "country": "3",
    "visits": 0
}, {
    "country": "4",
    "visits": 0
}, {
    "country": "5",
    "visits": 0
}, {
    "country": "6",
    "visits": 0
}, {
    "country": "7",
    "visits": 0
}, {
    "country": "8",
    "visits": 0
}, {
    "country": "9",
    "visits": 0
}, {
    "country": "10",
    "visits": 0
}, {
    "country": "11",
    "visits": 0
}, {
    "country": "12",
    "visits": 0
}, {
    "country": "13",
    "visits": 0
}, {
    "country": "14",
    "visits": 0
}, {
    "country": "15",
    "visits": 0
}, {
    "country": "16",
    "visits": 0
}, {
    "country": "17",
    "visits": 0
}, {
    "country": "18",
    "visits": 0
}, {
    "country": "19",
    "visits": 0
}, {
    "country": "20",
    "visits": 0
}, {
    "country": "21",
    "visits": 0
}, {
    "country": "22",
    "visits": 0
}, {
    "country": "23",
    "visits": 0
}, {
    "country": "24",
    "visits": 0
}, {
    "country": "25",
    "visits": 0
}, {
    "country": "26",
    "visits": 0
}, {
    "country": "27",
    "visits": 0
}, {
    "country": "28",
    "visits": 0
}, {
    "country": "29",
    "visits": 0
}, {
    "country": "30",
    "visits": 0
}, {
    "country": "31",
    "visits": 0
}];

$(function() { //initial fn ends here

    dashboarddata_fn();

});

//dashboard data fn starts here
function dashboarddata_fn() {
    $.ajax({
        url: dashboard_api,
        type: 'GET',
        headers: {
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {

            $(".profilereg").text(data.profiles_registered);
            $(".actmaleuser").text(data.active_male_users);
            $(".activefemaleuser").text(data.active_female_users);
            $(".photosapproved").text(data.photos_approved);

            $(".totregcount").text(data.today_registration);
            $(".totthisweek").text(data.this_week);
            $(".totthismnth").text(data.this_month);
            $(".totthisyear").text(data.this_year);
            var regcountinprecent = parseInt((data.today_registration / data.profiles_registered) * 100);
            $(".roundedvalue").attr("value", regcountinprecent);

            $(".knob").knob();

            if (data.recent_users.length != 0) {
                for (var i = 0; i < data.recent_users.length; i++) {
                    if (i < 5)

                        //         {
                        // "id": 63,
                        // "first_name": "Nancy",
                        // "last_name": "Lyn",
                        // "username": "8610905396",
                        // "email": "canadiantony1979@yahoo.ca",
                        // "is_active": true,
                        // "user_professional_details": {
                        //     "id": 42,
                        //     "occupation": {
                        //         "id": 5,
                        //         "name": "Executive"
                        //     }
                        // },
                        // "user_address": {
                        //     "id": 46,
                        //     "city": {
                        //         "id": 3180,
                        //         "name": "Chennai"
                        //     }
                        // }

                        $(".recentusers_tbody").append(`
                                    <tr>
                                                        <td>${i+1}</td>
                                                        <td>${data.recent_users[i].first_name} ${data.recent_users[i].last_name}</td>
                                                        <td>${data.recent_users[i].email}</td>
                                                        <td>${data.recent_users[i].username}</td>
                                                        <td>${(data.recent_users[i].user_address ? data.recent_users[i].user_address.city.name : "Not Mentioned")}</td>
                                                        <td><span class="label label-${(data.recent_users[i].is_active?"success":"pink")}">${(data.recent_users[i].is_active?"Active":"Deactive")}</span></td>
                                    </tr>
                    `);
                } //for loop ends here
            } //else cond ends here

            for (var i = 0; i < data.reg_per_day.length; i++) {
                dayscount[data.reg_per_day[i].day].visits = data.reg_per_day[i].count;
            }

            //chart fn starts here
            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "dark",
                "dataProvider": dayscount,
                "valueAxes": [{
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0
                }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "[[category]]: <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "visits"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "country",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20
                },
                "export": {
                    "enabled": true
                }

            }); //chart fn ends here

        },
        error: function(edata) {
            console.log("error occured in upcoming dashboard");
        }
    });

} //dashboard data fn ends here