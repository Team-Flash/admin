// var domain = "http://192.168.10.51:8000/";
var domain = "http://matrimony.billioncart.in/";

//auth api
var login_api = domain + 'auth/login/';
var forgotpwd_api = domain + 'auth/forgot-password/';
var resetpassword_api = domain + 'auth/reset-password/';
var changepassword_api = domain + 'auth/change-password/';
var logout_api = domain + 'auth/logout/';

//users list page api
var listusers_api = domain + 'admin/list/users/';
var userstatuschange = domain + 'admin/delete/user/';

//dashboard page api starts here
var dashboard_api = domain + 'admin/dashboard/';

//view user profile page api
var viewdprofile_api = domain + 'user/list/my-profile/';

//subadmin page api 
var addsubadmin_api = domain + 'admin/create/sub-admin/';
var listsubadmin_api = domain + 'admin/list/sub-admins/';
var editsubadmin_api = domain + 'admin/retrieve/update/sub-admin/';
var resetpwd_api = domain + 'admin/reset-password/';

//promotions page api starts here
var getpromotionslist_api = domain + 'admin/list/promotions/';
var promotioncreate_api = domain + 'admin/create/promotion/';
var retrivelist_api = domain + 'admin/update/retrieve/destroy/promotion/';
var updateeditprofile_api = domain + 'admin/update/retrieve/destroy/promotion/';
var deleteprofile_api = domain + 'admin/update/retrieve/destroy/promotion/';

//download as csv
var downloadascsv_api = domain + 'admin/download/users-details/';