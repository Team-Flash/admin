$(function() {

    (localStorage.userrole == 1) ? $(".addsubadminBtn").show() : $(".addsubadminBtn").hide();

    $(".addadminldr,.editadminldr,.ldmricon,.statusupdateldr,.resetpwdldr").hide();

    listsubadmin(0);

    $('#addemailid,#editemailid').focusout(function() {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^â€‹_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._â€‹~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!pattern.test($(this).val())) {
            var x = $(this).val();
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                $("#snackbarerror").text("Valid Email ID is required");
                showerrtoast();
                event.stopPropagation();
                return;
            }
        }
    });

    //add subadmin validation 
    $(".ipclksubadminadd").keyup(function(event) {
        event.keyCode == 13 ? $(".etrclksuadminadd").click() : "";
        $(this).val() ? $(this).removeClass('iserror') : $(this).addClass('iserror')
    });

    //edit subadmin validation 
    $(".ipclksuadminedit").keyup(function(event) {
        event.keyCode == 13 ? $(".etrclksubadminedit").click() : "";
        $(this).val() ? $(this).removeClass('iserror') : $(this).addClass('iserror')
    });

    //edit subadmin validation 
    $(".ipclkrspwd").keyup(function(event) {
        event.keyCode == 13 ? $(".etrclkrspwd").click() : "";
        $(this).val() ? $(this).removeClass('iserror') : $(this).addClass('iserror')
    });

});

$(".ipfield").keyup(function() {
    event.keyCode == 13 ? $(".searchclick").click() : "";
    $(this).val() ? $(this).removeClass('iserror') : $(this).addClass('iserror')
});

function addsubadmin_fn() {

    if ($("#addfname").val() == "") {
        $("#snackbarerror").text("First Name is required");
        $('#addfname').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#addemailid").val() == "") {
        $("#snackbarerror").text("Email ID is required");
        $('#addemailid').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    var email = $('#addemailid').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        $("#snackbarerror").text("Valid Email ID is required");
        $('#addemailid').addClass("iserror");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#addcntctno").val() == "") {
        $("#snackbarerror").text("Contact No is required");
        $('#addcntctno').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }


    if ($("#addpassword").val() == "") {
        $("#snackbarerror").text("Password is required");
        $('#addpassword').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#addpassword").val().length < 6) {
        $("#snackbarerror").text("Atleast 6 character is required for password");
        $('#addpassword').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".addadminldr").show();
    $(".addadminBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "first_name": $("#addfname").val(),
        "email": $("#addemailid").val(),
        "username": $("#addcntctno").val(),
        "password": $("#addpassword").val()
    });

    $.ajax({
        url: addsubadmin_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $(".addadminldr").hide();
            $(".addadminBtn").attr("disabled", false);

        },
        error: function(data) {

            $(".addadminldr").hide();
            $(".addadminBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Subadmin Added Successfully!");
        showsuccesstoast();
        // listsubadmin();
        $(".ipclksubadminadd").val("");
        $(".addsubadminclose").click();

    }); //done fn ends here

}

//user history fn starts here
function listsubadmin(type) {

    if (type == 0) {

        var url = listsubadmin_api;

    } else if (type == 1) {

        var url = sessionStorage.nexturl_subadmin;
        $(".ldmricon").show();

    } else {

        var url = listsubadmin_api + "?search=" + $(".searchquery").val();
    }

    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {

            (type == 0 || type == 2) ? $(".subadminlist_tbody").empty(): "";

            if (data.results.length == 0 && (type == 0 || type == 2)) {
                // $(".userlist_table").hide();
                // $(".user_nodata").show();
                $(".ldmrdiv,.showall").hide();
            } else {
                $(".ldmrdiv").show();

                if (data.next_url != null) {
                    sessionStorage.nexturl_subadmin = data.next_url;
                    $(".ldmrdiv").show();
                } else {
                    $(".ldmrdiv").hide();
                }

                for (var i = 0; i < data.results.length; i++) {

                    //            {
                    //     "id": 67,
                    //     "username": "1234657980",
                    //     "first_name": "Krishna",
                    //     "email": "krishna001@gmail.com",
                    //     "is_active": true
                    // }

                    $(".subadminlist_tbody").append(`

                        <tr class="${(data.results[i].is_active?"":"subadmindisabled")}">
                                            <td class="uname${data.results[i].id}">${data.results[i].first_name}</td>
                                            <td class="uemail${data.results[i].id}">${data.results[i].email}</td>
                                            <td class="uphone${data.results[i].id}">${data.results[i].username}</td>
                                            <td>
                                                <center>
                                                    <button type="button" onclick="resetpassword(${data.results[i].id})" class="btn btn-white btn-custom btn-rounded waves-effect tablebtn" data-animation="flip" data-toggle="modal" data-target="#resetpwdmodal" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a">Click Here</button>
                                                </center>
                                            </td>
                                            <td>
                                                <button class="btn btn-icon waves-effect waves-light btn-warning tableaction" onclick="editmdldata(${data.results[i].id})" data-toggle="modal" data-target="#editsubadminmodal"> <i class="typcn typcn-pencil"></i> </button>
                                                <button class="btn btn-icon waves-effect waves-light btn-danger tableaction" onclick="statucchange(${data.results[i].id})" data-toggle="modal" data-target="#deletesubadminmodal"> <i class="fa fa-trash"></i> </button>
                                            </td>
                    </tr>
                    `);



                }
                $(".ldmricon,.showall").hide();
            }
        },
        error: function(edata) {
            console.log("error occured in user history page");
            // $(".userlist_tbody").empty();
            // $(".userlist_table").hide();
            // $(".user_nodata").show();
            $(".ldmrdiv,.ldmricon,.showall").hide();
        }
    });
}

//search fn starts here
function search_fn() {

    if ($('.searchquery').val().trim() == '') {
        $("#snackbarerror").text("Search Query is required");
        $('.searchquery').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    listsubadmin(2);

}

//show all fn starts here
function showall() {

    listsubadmin(0);
    $(".showall").show();
    $(".searchquery").val("");

}

//edit data get fn starts here
function editmdldata(userid){
    sessionStorage.subadminedit_id = userid;
    $("#editfname").val($(".uname"+userid).text());
    $("#editemailid").val($(".uemail"+userid).text());
    $("#editcntctno").val($(".uphone"+userid).text());
}

function editsubadmin(){

     if ($("#editfname").val() == "") {
        $("#snackbarerror").text("First Name is required");
        $('#addfname').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".editadminldr").show();
    $(".editadminBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "first_name": $("#editfname").val(),
        "email": $("#editemailid").val(),
        "username": $("#editcntctno").val()
    });

    $.ajax({
        url: editsubadmin_api + sessionStorage.subadminedit_id + '/',
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $(".editadminldr").hide();
            $(".editadminBtn").attr("disabled", false);

        },
        error: function(data) {

            $(".editadminldr").hide();
            $(".editadminBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Subadmin Edited Successfully!");
        showsuccesstoast();
        listsubadmin(0);
        $(".ipclksuadminedit").val("");
        $(".editsubadminclose").click();

    }); //done fn ends here

}

//status change fn starts here
function statucchange(userid) {
    sessionStorage.statuschange_id = userid;
}

function statuschangefinal() {

    $(".statusupdateldr").show();
    $(".statusupdateBtn").attr("disabled", true);

    $.ajax({
        url: userstatuschange + sessionStorage.statuschange_id + '/',
        type: 'DELETE',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.statusupdateldr').hide();
            $(".statusupdateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.statusupdateldr').hide();
            $(".statusupdateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        listsubadmin(0);
        $(".cnfrmmdlclose").click();
        $("#snackbarsuccs").text("Status has been updated successfully for " + $(".uname" + sessionStorage.statuschange_id).text());
        showsuccesstoast();

    });

}//status change fn starts here

//resetpassword fn starts here
function resetpassword(userid) {
    sessionStorage.resetpwduser_id = userid;
}

function resetpassword_final(){

    if ($("#rspassword").val() == "") {
        $("#snackbarerror").text("Password is required");
        $('#rspassword').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#rspassword").val().length < 6) {
        $("#snackbarerror").text("Atleast 6 character is required for password");
        $('#rspassword').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#rsrenewpassword").val() == "") {
        $("#snackbarerror").text("Password is required");
        $('#rsrenewpassword').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#rsrenewpassword").val().length < 6) {
        $("#snackbarerror").text("Atleast 6 character is required for password");
        $('#rsrenewpassword').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#rspassword').val() != $('#rsrenewpassword').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".resetpwdldr").show();
    $(".resetpwdBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "password": $("#rspassword").val()
    });

    $.ajax({
        url: resetpwd_api + sessionStorage.resetpwduser_id + '/',
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $(".resetpwdldr").hide();
            $(".resetpwdBtn").attr("disabled", false);

        },
        error: function(data) {

            $(".resetpwdldr").hide();
            $(".resetpwdBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Password has been reset successfully for "+ $(".uname" + sessionStorage.resetpwduser_id).text());
        showsuccesstoast();
        $(".ipclkrspwd").val("");
        $(".rspwdmdlclose").click();

    }); //done fn ends here

}