$(function() {

    $.ajax({
        url: viewdprofile_api + sessionStorage.profileuser_id + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {

            localStorage.myprofiledata = JSON.stringify(data);
            gotoshowprofile_fn();

        },
        error: function(edata) {
            console.log("error occured in my profile data");
        }
    });

});

// gotoshowprofile_fn fn starts headers
function gotoshowprofile_fn() {

    var data = JSON.parse(localStorage.myprofiledata);

    $(".dynotherimages").empty();
    if (data.user_photos.length >= 1) {

        $(".mainimage").attr("src", data.user_photos[0].image);

        for (var i = 1; i < data.user_photos.length; i++) {
            $(".dynotherimages").append(`
                <a href="#">
                        <img src="${data.user_photos[i].image}" class="img-circle thumb-md" alt="friend">
                </a>
                `)
        }
    } else {
        $(".dynotherimages").append('<p class="noimginuserprof">No Images Found</p>')
    }

    //name , age ,gender , about them ,mailid , phoneno
    var gentereq = (data.userdetails.gender == "Male") ? "Mr. " : "Ms. ";
    $(".dynusername").text(gentereq + data.first_name + " " + data.last_name);

    var currentyear = parseInt(new Date().getFullYear());
    var userage = currentyear - parseInt(data.userdetails.dob.substring(0, 4));
    $(".dynuserage").text(userage);
    $(".dynusergender").text(data.userdetails.gender);

    var userdob = data.userdetails.dob.split("-");
    userdob = userdob[2] + "-" + userdob[1] + "-" + userdob[0];

    $(".dynuserdob").text(userdob);

    (data.about_me != null) ? $(".dynuseraboutthem").text(data.about_me.about.substring(0, 40)): $(".dynuseraboutthem").text("Not Mentioned");

    $(".dynuserphoneno").text(data.username);
    $(".dynuseremailid").text(data.email).attr("mailto", "tel:" + data.email + "");
    // ==================================================================================================================

    //PERSONEL DETAILS SECTION

    $(".dyndob").text(data.userdetails.dob);

    if (data.user_personal_details != null) {
        //marital status fn
        $(".dynmaritalstatus").text(data.user_personal_details.marital_status.name);

        //user height fn
        var userheight = data.user_personal_details.height.toString().split(".");
        if (userheight.length == 1) {
            var showuserheight = userheight[0] + " ft";
        } else {
            var showuserheight = userheight[0] + " ft " + userheight[1] + " in";
        }
        $(".dynuserheight").text(showuserheight);

        //family status , family values , family type fn
        $(".dynuserfam_status").text(data.user_personal_details.family_status.name);
        $(".dynuserfam_value").text(data.user_personal_details.family_values.name);
        $(".dynuserfam_type").text(data.user_personal_details.family_type);
        (data.user_personal_details.is_physically_challenged == true) ? $(".dynuser_disability").text("Yes"): $(".dynuser_disability").text("No");

    } else {
        $(".dynmaritalstatus").text("Not mentioned");
        $(".dynuserheight").text("Not mentioned");
        $(".dynuserfam_status").text("Not mentioned");
        $(".dynuserfam_value").text("Not mentioned");
        $(".dynuserfam_type").text("Not mentioned");
        $(".dynuser_disability").text("Not mentioned");
    }

    //Professional Details
    if (data.user_professional_details != null) {
        $(".dynuserhighstedu").text(data.user_professional_details.highest_education.name);
        $(".dynuseremployedin").text(data.user_professional_details.employed_in.name);
        $(".dynuseroccupation").text(data.user_professional_details.occupation.name);
        if (data.user_professional_details.annual_income == 1) {
            var userincome = "1 to 3 lakhs ";
        } else if (data.user_professional_details.annual_income == 2) {
            var userincome = "3 to 5 lakhs ";
        } else if (data.user_professional_details.annual_income == 3) {
            var userincome = "5 to 10 lakhs ";
        } else {
            var userincome = "10 lakhs and more";
        }
        $(".dynuserincome").text(data.user_professional_details.currency.name + " " + userincome + " per annum");
    } else {
        $(".dynuserhighstedu").text("Not mentioned");
        $(".dynuseremployedin").text("Not mentioned");
        $(".dynuseroccupation").text("Not mentioned");
        $(".dynuserincome").text("Not mentioned");
    }

    //Hobbies & Interests details
    if (data.hobbies.length != 0) {
        var userhobbies = "";
        for (var i = 0; i < data.hobbies.length; i++) {
            userhobbies += data.hobbies[i].hobby.name + " , ";
        }
        $(".dynuserhobbies").text(userhobbies.slice(0, -2) + ".");
    } else {
        $(".dynuserhobbies").text("Not mentioned")
    }

    if (data.favourite_music.length != 0) {
        var favmusics = "";
        for (var i = 0; i < data.favourite_music.length; i++) {
            favmusics += data.favourite_music[i].music.name + " , ";
        }
        $(".dynuserfavmusic").text(favmusics.slice(0, -2) + ".");
    } else {
        $(".dynuserfavmusic").text("Not mentioned")
    }

    if (data.sports.length != 0) {
        var favsports = "";
        for (var i = 0; i < data.sports.length; i++) {
            favsports += data.sports[i].sport.name + " , ";
        }
        $(".dynuserfavsports").text(favsports.slice(0, -2) + ".");
    } else {
        $(".dynuserfavsports").text("Not mentioned")
    }

    if (data.spoken_languages.length != 0) {
        var spokenlang = "";
        for (var i = 0; i < data.spoken_languages.length; i++) {
            spokenlang += data.spoken_languages[i].language.name + " , ";
        }
        $(".dynuserspokenlang").text(spokenlang.slice(0, -2) + ".");
    } else {
        $(".dynuserspokenlang").text("Not mentioned")
    }

    // Basic Information Details
    if (data.basic_information != null) {
        $(".dynuserbodytype").text(data.basic_information.body_type);
        $(".dynuserskintone").text(data.basic_information.skin_tone);
        $(".dynuserweight").text(data.basic_information.weight + " Kgs");
    } else {
        $(".dynuserbodytype").text("Not mentioned");
        $(".dynuserskintone").text("Not mentioned");
        $(".dynuserweight").text("Not mentioned");
    }

    if (data.lifestyle != null) {
        $(".dynusereatinghabits").text(data.lifestyle.eating_habit);
        $(".dynuserdrinkinghabits").text(data.lifestyle.drinking_habit);
        $(".dynusersmokinghabits").text(data.lifestyle.smoking_habit);
    } else {
        $(".dynusereatinghabits").text("Not mentioned");
        $(".dynuserdrinkinghabits").text("Not mentioned");
        $(".dynusersmokinghabits").text("Not mentioned");
    }

    if (data.family_info != null) {
        $(".dynuserfatherstatus").text(data.family_info.father_status.name);
        $(".dynusermotherstatus").text(data.family_info.mother_status.name);
        $(".dynuserparentphnno").text(data.family_info.contact_number);
    } else {
        $(".dynuserfatherstatus").text("Not mentioned");
        $(".dynusermotherstatus").text("Not mentioned");
        $(".dynuserparentphnno").text("Not mentioned");
    }

    // Religious Details
    $(".dynuserreligion").text(data.userdetails.religion.name);
    $(".dynusermothertongue").text(data.userdetails.mother_tongue.name);

    if (data.user_religion_details != null) {
        //user caste , subcaste , gothra , Dosham
        $(".dynusercaste").text(data.user_religion_details.caste.name);

        if (data.user_religion_details.subcaste != null) {
            $(".dynusersubcaste").text(data.user_religion_details.subcaste.name);
        } else {
            $(".dynusersubcaste").text("Not mentioned");
        }

        (data.user_religion_details.gothram != null) ? $(".dynusergothra").text(data.user_religion_details.gothram): $(".dynusergothra").text("Not mentioned");

        if (data.user_dosham.length != 0) {
            var userdosham = "";
            for (var i = 0; i < data.user_dosham.length; i++) {
                userdosham += data.user_dosham[i].dosham.name + " , ";
            }
            $(".dynuserdosham").text(data.user_religion_details.dosham_choices + " - " + userdosham.slice(0, -2) + ".");
        } else {
            $(".dynuserdosham").text(data.user_religion_details.dosham_choices);
        }

    } else {
        $(".dynusercaste").text("Not mentioned");
        $(".dynusersubcaste").text("Not mentioned");
        $(".dynusergothra").text("Not mentioned");
        $(".dynuserdosham").text("Not mentioned");
    }

    if (data.religion_info != null) {
        $(".dynuserstar").text(data.religion_info.star.name);
        $(".dynuserraasi").text(data.religion_info.raasi.name);
    } else {
        $(".dynuserstar").text("Not mentioned");
        $(".dynuserraasi").text("Not mentioned");
    }

    //PARTNER DETAILS 
    if (data.partner_basic_info != null) {

        //partner starts age and end age
        // $(".dynuserpartnerage").text(data.partner_basic_info.from_age + " to " + data.partner_basic_info.to_age);
        $(".dynuserp_startage").text(data.partner_basic_info.from_age);
        $(".dynuserp_endage").text(data.partner_basic_info.to_age);

        //partner starts height and end height
        var userp_startheight = data.partner_basic_info.from_height.toString().split(".");
        if (userp_startheight.length == 1) {
            var showuserp_startheight = userp_startheight[0] + " ft";
        } else {
            var showuserp_startheight = userp_startheight[0] + " ft " + userp_startheight[1] + " in";
        }
        $(".dynuserp_startheight").text(showuserp_startheight);

        var userp_endheight = data.partner_basic_info.to_height.toString().split(".");
        if (userp_endheight.length == 1) {
            var showuserp_endheight = userp_endheight[0] + " ft";
        } else {
            var showuserp_endheight = userp_endheight[0] + " ft " + userp_endheight[1] + " in";
        }
        $(".dynuserp_endheight").text(showuserp_endheight);
        // $(".dynuserpartnerheight").text(showuserp_startheight + " to " + showuserp_endheight);

        //partner starts weight and end weight
        // $(".dynuserpartnerweight").text(data.partner_basic_info.from_weight + " Kgs" + " to " + data.partner_basic_info.to_weight + " Kgs");
        $(".dynuserp_startweight").text(data.partner_basic_info.from_weight + " Kgs");
        $(".dynuserp_endweight").text(data.partner_basic_info.to_weight + " Kgs");

        //partner marital status
        if (data.marital_status.length != 0) {
            var partnermaritalstatus = "";
            for (var i = 0; i < data.marital_status.length; i++) {
                partnermaritalstatus += data.marital_status[i].marital_status.name + " , ";
            }
            $(".dynuserp_maritalstatus").text(partnermaritalstatus.slice(0, -2) + ".");
        } else {
            $(".dynuserp_maritalstatus").text("Not mentioned");
        }

        //physical status fn
        if (data.partner_basic_info.physical_status == 1) {
            $(".dynuserp_physicalstatus").text("Normal");
        } else if (data.partner_basic_info.physical_status == 2) {
            $(".dynuserp_physicalstatus").text("Physically Challenged");
        } else {
            $(".dynuserp_physicalstatus").text("Doesn't matter");
        }

        $(".dynuserp_eatinghabits").text(data.partner_basic_info.eating_habit);
        $(".dynuserp_drinkinghabits").text(data.partner_basic_info.drinking_habit);
        $(".dynuserp_smokinghabits").text(data.partner_basic_info.smoking_habit);

    } else {
        $(".dynuserpartnerage").text("Not mentioned");
        $(".dynuserpartnerheight").text("Not mentioned");
        $(".dynuserpartnerweight").text("Not mentioned");
        $(".dynuserp_physicalstatus").text("Not mentioned");
        $(".dynuserp_eatinghabits").text("Not mentioned");
        $(".dynuserp_drinkinghabits").text("Not mentioned");
        $(".dynuserp_smokinghabits").text("Not mentioned");
        $(".dynuserp_maritalstatus").text("Not mentioned");
    }

    //partner Religion
    if (data.partner_religion.length != 0) {
        var partnerreligion = "";
        for (var i = 0; i < data.partner_religion.length; i++) {
            partnerreligion += data.partner_religion[i].religion.name + " , ";
        }
        $(".dynuserp_religion").text(partnerreligion.slice(0, -2) + ".");
    } else {
        $(".dynuserp_religion").text("Not mentioned");
    }

    //partner Mother Tongue
    if (data.partner_mother_tongue.length != 0) {
        var partnermothertongue = "";
        for (var i = 0; i < data.partner_mother_tongue.length; i++) {
            partnermothertongue += data.partner_mother_tongue[i].mother_tongue.name + " , ";
        }
        $(".dynuserp_mothertongue").text(partnermothertongue.slice(0, -2) + ".");
    } else {
        $(".dynuserp_mothertongue").text("Not mentioned");
    }

    //partner caste
    if (data.partner_caste != null) {
        $(".dynuserp_caste").text(data.partner_caste.caste.name);
        $(".dynuserp_manglik").text(data.partner_caste.manglik);
    } else {
        $(".dynuserp_caste").text("Not Mentioned");
        $(".dynuserp_manglik").text("Not Mentioned");
    }

    //partner Star 
    if (data.partner_star.length != 0) {
        var partnerstar = "";
        for (var i = 0; i < data.partner_star.length; i++) {
            partnerstar += data.partner_star[i].star.name + " , ";
        }
        $(".dynuserp_star").text(partnerstar.slice(0, -2) + ".");
    } else {
        $(".dynuserp_star").text("Not mentioned");
    }

    //partner country 
    if (data.partner_country.length != 0) {
        var partnercountry = "";
        for (var i = 0; i < data.partner_country.length; i++) {
            partnercountry += data.partner_country[i].country.name + " , ";
        }
        $(".dynuserp_country").text(partnercountry.slice(0, -2) + ".");
    } else {
        $(".dynuserp_country").text("Not mentioned");
    }

    //partner citizenship 
    if (data.partner_citizenship.length != 0) {
        var partnercitizenship = "";
        for (var i = 0; i < data.partner_citizenship.length; i++) {
            partnercitizenship += data.partner_citizenship[i].citizenship.name + " , ";
        }
        $(".dynuserp_citizenship").text(partnercitizenship.slice(0, -2) + ".");
    } else {
        $(".dynuserp_citizenship").text("Not mentioned");
    }

    //partner education 
    if (data.partner_education.length != 0) {
        var partnereducation = "";
        for (var i = 0; i < data.partner_education.length; i++) {
            partnereducation += data.partner_education[i].education.name + " , ";
        }
        $(".dynuserp_education").text(partnereducation.slice(0, -2) + ".");
    } else {
        $(".dynuserp_education").text("Not mentioned");
    }

    //partner occupation 
    if (data.partner_occupation.length != 0) {
        var partneroccupation = "";
        for (var i = 0; i < data.partner_occupation.length; i++) {
            partneroccupation += data.partner_occupation[i].occupation.name + " , ";
        }
        $(".dynuserp_occupation").text(partneroccupation.slice(0, -2) + ".");
    } else {
        $(".dynuserp_occupation").text("Not mentioned");
    }

    //partner description
    if (data.partner_description != null) {
        $(".dynuserp_annualincome").text(data.partner_description.currency.name + " " + data.partner_description.income_from + " to " + data.partner_description.income_to + " per annum");
        $(".dynuserp_partnerdesc").text(data.partner_description.about_partner);
    } else {
        $(".dynuserp_annualincome").text("Not Mentioned");
        $(".dynuserp_partnerdesc").text("Not Mentioned");
    }

    if (data.user_address != null) {
        // $(".dynuseraddr").text(data.user_address.street + " , " + data.user_address.area + " , " + data.user_address.city.name + " , " + data.user_address.city.state.name + " , " + data.user_address.city.state.country.name + " , " + data.user_address.zipcode + " .");
        $(".dynuseraddr").text(data.user_address.city.name + " , " + data.user_address.city.state.name + " , " + data.user_address.city.state.country.name + " , " + data.user_address.zipcode + " .");
    } else {
        $(".dynuseraddr").text("Not Mentioned");
    }



}

//get user content fn starts here
function getaboutusercntnt() {

    var data = JSON.parse(localStorage.myprofiledata);
    (data.about_me != null) ? $(".dynusercontentinmdl").text(data.about_me.about): $(".dynusercontentinmdl").text("Not Mentioned");

}


//print profile
function printprofile() {
    var oldPage = document.body.innerHTML;
    var newpage = "<p>"+JSON.parse(localStorage.myprofiledata).userprofile.uid+"</p><img src='"+$(".mainimage").attr("src")+"' style='float:right;width:200px;height:200px'>";
    $(".card-box img").remove();
    $(".dynotherimages").remove();
    $(".header-title").remove();
    $(".abt_prt").remove();
    for (var i = 0; i < $(".card-box").length; i++) {
        newpage += $(".card-box").eq(i).html();
    }
    document.body.innerHTML =
        "<html><head><title></title></head><body>" +
        newpage + "</body>";
    //Print Page
    window.print();
    //Restore orignal HTML
    document.body.innerHTML = oldPage;
}


// //print profile
// function printprofile() {
//     var newpage;
//     $(".card-box img").remove();
//     $(".dynotherimages").remove();
//     $(".header-title").remove();
//     for (var i = 0; i < $(".card-box").length; i++) {
//         newpage += $(".card-box").eq(i).html();
//     }
//     var newWindow = window.open();

//     newWindow.document.body.innerHTML =
//         "<html><head><title></title></head><body>" +
//         newpage + "<script></script></body>";
//     newWindow.print();
//     //Print Page
//     //Restore orignal HTML
// }