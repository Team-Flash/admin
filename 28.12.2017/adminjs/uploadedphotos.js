 var sno = 0;
 $(function() { listUsers(0, '', '');
     $('#firsttable').empty(); });

 var nextURL = ``,
     prevURL = ``;

 function showAll() {
     $('#myInput').val('');
     $('#loaderspinner').fadeIn();
     listUsers(0, '', '');
 }

 function listUsers(nu = 0, urlD, searchparam = '') {
     var urlData = nu == 0 ? `${domain}admin/list/users/?type=2&` : nextURL;
     urlData += searchparam != '' ? searchparam : '';
     $.ajax({
         url: urlData,
         type: 'get',
         headers: {
             'Authorization': `Token ${localStorage.wutkn}`
         },
         success: function(data) {
             $('#loaderspinner').fadeOut();
             data.next_url ? (nextURL = data.next_url, $('#loadMore').show('')) : (nextURL = '', $('#loadMore').hide());
             var appendUsers = '';
             for (var user of data.results) {
                 sno = sno + 1;
                 appendUsers += `<tr><td>${sno}</td><td style="">${user.first_name} ${user.last_name}</td><td style="">${user.email}</td><td style="">${user.username}</td><td style=""><span class="label label-table">${user.user_address?user.user_address.city.name:'N/A'}</span></td><td style="">${user.user_professional_details?user.user_professional_details.occupation.name:'N/A'}</td><td class="text-center" style=""><center><button type="button" onclick="showPhotos(${user.id})" class="btn btn-white btn-custom btn-rounded waves-effect tablebtn" data-animation="flip" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a">Click Here</button></center></td></tr>`;
             }
             $('#firsttable').append(appendUsers);
             if (data.results.length == 0) {
                 $('#users-table').hide();
                 $('#snackbarerror').text('No Data Found!!');
                 showerrtoast();
             } else { $('#users-table').show(); }
         },
         error: edata => console.log(edata)
     });
 }

 function showPhotos(id) {
     $('#loaderspinner').fadeIn();
     $.ajax({
         url: `${domain}admin/list/uploaded-photos/${id}/`,
         type: 'get',
         headers: {
             'Authorization': `Token ${localStorage.wutkn}`
         },
         success: function(data) {
             $('#loaderspinner').fadeOut()
             var images = ``;
             if (data.length == 0) {
                 $('#snackbarerror').text('No photos to approve/disapprove');
                 showerrtoast();
             } else {
                 for (var image of data) {
                     images += `<div class="col-md-3 user-photo-container" ><div class="show-image"><img src="${image.image}" class="img-responsive proofimages"><p class="update"><a onclick="approvePhoto(${image.id},true,this)" class="label label-table label-default" href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></p><p class="delete"><a onclick="approvePhoto(${image.id},false,this)" class="label label-table label-info" href="#"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a></p></div></div>`;
                 }
                 $('#image-data').empty();
                 $('#image-data').append(images);
                 $('#additionaldetailsmodal').modal('show');
             }
         },
         error: edata => console.log(edata)
     });
 }

 function approvePhoto(id, approve, me) {
     $('#loaderspinner').fadeIn();
     var postData = { "is_approved": approve };
     $.ajax({
         url: `${domain}admin/approve-photos/${id}/`,
         type: 'put',
         data: JSON.stringify(postData),
         headers: {
             'Authorization': `Token ${localStorage.wutkn}`,
             'content-type': 'application/json'
         },
         success: function(data) {
             $(me).closest('.user-photo-container').remove();
             $('#snackbarsuccs').text(approve ? "Approved a photo" : "Disapproved a photo");
             showsuccesstoast();
             $('#loaderspinner').fadeOut();
             if ($('#image-data .user-photo-container').length == 0) {
                 $('#additionaldetailsmodal').modal('hide');
             }
         },
         error: edata => console.log(edata)
     });
 }

 function searchData(e) {
     e.preventDefault();
     var searchParams = ``;
     if ($('#myInput').val()) { searchParams += `search=${$('#myInput').val()}` }
     sno = 0;
     listUsers(0, '', searchParams);
     $('#firsttable').empty();
 }

 function loadNext() {
     if (nextURL != '') { listUsers(1, nextURL);
         $('#loaderspinner').fadeIn(); }
 }