 var sno = 0;
 $(function() {

    $('.button-menu-mobile').click();

     $(".ldmrdiv,.user_nodata,.showall,.statusupdateldr,.csvldr").hide();

     userlist_fn(0);

 });


 //user history fn starts here
 function userlist_fn(type) {

     if (type == 0) {

         var url = listusers_api+"?type=1";

     } else if (type == 1) {

         var url = sessionStorage.nexturl_userhistory;
         $(".ldmricon").show();

     } else {

         var url = listusers_api + "?type=1&search=" + $(".searchquery").val();
     }

     $.ajax({
         url: url,
         type: 'GET',
         headers: {
             'Authorization': 'Token ' + localStorage.wutkn
         },
         success: function(data) {

             if (type == 0 || type == 2) {
                 $(".userlist_tbody").empty();
                 sno = 0;
             }

             if (data.results.length == 0 && (type == 0 || type == 2)) {
                 $(".userlist_table").hide();
                 $(".user_nodata").show();
                 $(".ldmrdiv,.showall").hide();
                 sno = 0;
             } else {
                 $(".userlist_table").show();
                 $(".user_nodata").hide();
                 $(".ldmrdiv").show();

                 if (data.next_url != null) {
                     sessionStorage.nexturl_userhistory = data.next_url;
                     $(".ldmrdiv").show();
                 } else {
                     $(".ldmrdiv").hide();
                 }

                 for (var i = 0; i < data.results.length; i++) {

                     //             {
                     //     "id": 8,
                     //     "first_name": "Gopi",
                     //     "last_name": "Akshay",
                     //     "username": "8610011892",
                     //     "email": "gopi@billiontags.com",
                     //     "is_active": true,
                     //     "user_professional_details": {
                     //         "id": 2,
                     //         "occupation": {
                     //             "id": 34,
                     //             "name": "Designer"
                     //         }
                     //     },
                     //     "user_address": {
                     //         "id": 2,
                     //         "city": {
                     //             "id": 3180,
                     //             "name": "Chennai"
                     //         }
                     //     }
                     // }
                     sno = sno + 1;
                     $(".userlist_tbody").append(`
                         <tr>   
                                <td>${sno}</td>
                                <td>${data.results[i].date_joined.slice(0,data.results[i].date_joined.indexOf("T"))}</td>
                                <td>${(data.results[i].last_login?data.results[i].last_login.slice(0,data.results[i].last_login.indexOf("T")) : "Nil" )}</td>
                                <td class="uname${data.results[i].id}">${data.results[i].first_name} ${data.results[i].last_name}</td>
                                <td>${data.results[i].email}</td>
                                <td>${data.results[i].username}</td>
                                <td>${(data.results[i].user_address ? data.results[i].user_address.city.name : "Nil")} / ${(data.results[i].user_address ? data.results[i].user_address.city.state.name : "Nil")}</td>
                                <td>${(data.results[i].user_professional_details ? data.results[i].user_professional_details.occupation.name : "Nil")}</td>
                                <td>
                                    <center>
                                        <a type="button" class="btn btn-white btn-custom btn-rounded waves-effect tablebtn" onclick="gotoviewprofile(${data.results[i].id})">Click Here</a>
                                    </center>
                                </td>
                                <td class="statusBtn${data.results[i].id}"><a onclick="statucchange(${data.results[i].id})" data-target="#confirmationmodal" data-toggle="modal"><span class="label label-table label-${(data.results[i].is_active?"success":"danger")} pointer">${(data.results[i].is_active?"Deactivate":"Activate")}</span></a></td>
                        </tr>

                    `);

                 }
                 $(".ldmricon,.showall").hide();
             }
         },
         error: function(edata) {
             console.log("error occured in user history page");
             $(".userlist_tbody").empty();
             $(".userlist_table").hide();
             $(".user_nodata").show();
             $(".ldmrdiv,.ldmricon,.showall").hide();
         }
     });

 }

 $(".ipfield").keyup(function() {
     event.keyCode == 13 ? $(".searchclick").click() : "";
     $(this).val() ? $(this).removeClass('iserror') : $(this).addClass('iserror')
 });

 //search fn starts here
 function search_fn() {

     // if ($('.searchquery').val().trim() == '') {
     //     $("#snackbarerror").text("Search Query is required");
     //     $('.searchquery').addClass("iserror");
     //     showiperrtoast();
     //     event.stopPropagation();
     //     return;
     // }

     userlist_fn(2);

 }

 //show all fn starts here
 function showall() {

     userlist_fn(0);
     $(".showall").show();
     $(".searchquery").val("");

 }

 //profile view fn starsts here
 function gotoviewprofile(userid) {
     sessionStorage.profileuser_id = userid;
     window.location.href = "view-profile.html";
 }

 //status change fn starts here
 function statucchange(userid) {
     sessionStorage.statuschange_id = userid;
 }

 function statuschangefinal() {

     $(".statusupdateldr").show();
     $(".statusupdateBtn").attr("disabled", true);

     $.ajax({
         url: userstatuschange + sessionStorage.statuschange_id + '/',
         type: 'DELETE',
         headers: {
             "content-type": 'application/json',
             "Authorization": "Token " + localStorage.wutkn
         },
         success: function(data) {

             $('.statusupdateldr').hide();
             $(".statusupdateBtn").attr("disabled", false);

         },
         error: function(data) {

             $('.statusupdateldr').hide();
             $(".statusupdateBtn").attr("disabled", false);

             var errtext = "";
             for (var key in JSON.parse(data.responseText)) {
                 errtext = JSON.parse(data.responseText)[key][0];
             }
             $("#snackbarerror").text(errtext);
             showerrtoast();

         }
     }).done(function(dataJson) {
         // userlist_fn(0);
         dataJson.is_active ? $(".statusBtn" + sessionStorage.statuschange_id).empty().append('<a onclick="statucchange(' + sessionStorage.statuschange_id + ')" data-target="#confirmationmodal" data-toggle="modal"><span class="label label-table label-success pointer">Deactivate</span></a>') : $(".statusBtn" + sessionStorage.statuschange_id).empty().append('<a onclick="statucchange(' + sessionStorage.statuschange_id + ')" data-target="#confirmationmodal" data-toggle="modal"><span class="label label-table label-danger pointer">Activate</span></a>');

         $(".cnfrmmdlclose").click();
         $("#snackbarsuccs").text("Status has been updated successfully for " + $(".uname" + sessionStorage.statuschange_id).text());
         showsuccesstoast();
     });

 } //status change fn starts here

//download as csv
function downloadcsv(){
    $(".csvldr").show();
    $(".csvbtn").attr("disabled", true);
    $.ajax({
        url: downloadascsv_api,
        type: 'GET',
        headers: {
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data){
            $(".csvldr").hide();
            $(".csvbtn").attr("disabled", false);
            window.location.href = data.data;
        },
        error: function(data) {
            $(".csvldr").hide();
            $(".csvbtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}