$(function() {
    $('.submitldr,.updateimgmodal,.updateimodal,.deletemodal,.ldreditprpty,.load_btn1').hide();
    onloaddata(0);
})

//initial load function
function onloaddata(val) {
    if (val == 0) {
        $('.appendcards').empty();
        var url = getpromotionslist_api;
    } else {
        $('.ldreditprpty').show();
        var url = localStorage.nextpage;
    }
    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('.ldreditprpty').hide();
            for (var i = 0; i < data.results.length; i++) {
                if (data.results[i].description) { 
                    $('.appendcards').append(`<div class="column size-1of3"><div class="card-grid-col"><article class="card-typical"><div class="card-typical-section"><div class="user-card-row"><div class="tbl-row"><div class="tbl-cell"><p class="user-card-row-name"><a class="restnameview${data.results[i].id}" href="#">${data.results[i].name}</a></p></div><div class="tbl-cell tbl-cell-status "><a href="#" data-toggle="modal" onclick="editdescription('${data.results[i].id}');" class="font-icon md-trigger"><i class="fa fa-pencil" aria-hidden="true"></i></a><a class="font-icon" data-toggle="modal" onclick="deleteemployee('${data.results[i].id}')" data-target="#deletesubadminmodal"><i class="fa fa-trash-o " aria-hidden="true "></i></a></div></div></div></div><div class="card-typical-section card-typical-content"><p>${data.results[i].description}</p></div></article></div></div>`);
                } else if (data.results[i].image) {
                    $('.appendcards').append(`<div class="column size-1of3"><div class="card-grid-col "><article class="card-typical "><div class="card-typical-section "><div class="user-card-row "><div class="tbl-row "><div class="tbl-cell "><p class="user-card-row-name "><a class="restnameview${data.results[i].id}" href="# ">${data.results[i].name}</a></p></div><div class="tbl-cell tbl-cell-status "><a href="#" data-toggle="modal" onclick="editdescriptionimg('${data.results[i].id}')" class="font-icon md-trigger"><i class="fa fa-pencil" aria-hidden="true"></i></a><a class="font-icon" data-toggle="modal" onclick="deleteemployee('${data.results[i].id}')" data-target="#deletesubadminmodal"><i class="fa fa-trash-o " aria-hidden="true "></i></a></div></div></div></div><div class="card-typical-section card-typical-content"><div class="photo"><img src="${data.results[i].image}" alt=""></div></div></article></div></div>`);
                }
            }
            if (data.next_url != null) {
                localStorage.nextpage = data.next_url;
                $('.load_btn1').show();
            } else {
                $('.load_btn1').hide();

            }

        },
        error: function(data) {
            console.log("Error Occured in employee role loading");
        }
    });
} //onload fn ends here


//on update content function starts here

function update_content() {

    var profimagefile = $(".add_pro")[0].files;

    if ($('.addname').val().trim() == '') {
        $('.addname').addClass("iserror");
        $("#snackbarerror").text("Name is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('.adddescription').val().trim() == '' && (profimagefile.length == 0)) {
        $("#snackbarerror").text("Description or Image is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    var postData = new FormData();
    postData.append("name", $('.addname').val());

    if ($('.adddescription').val().trim() != '') {
        postData.append("description", $('.adddescription').val());
    }
    if (profimagefile.length != 0) {
        postData.append("image", profimagefile[0]);
    }
    $('.submitldr').show();
    $.ajax({
            url: promotioncreate_api,
            type: 'POST',
            data: postData,
            crossDomain: true,
            contentType: false,
            processData: false,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn);
            },
            success: function(data) {
                $('.submitldr').hide();
                $('.submitldr').attr("disabled", false);
                $(".cncl").click();
            },
            error: function(data) {

                $('.submitldr').hide();
                $('.submitldr').attr("disabled", false);
                var errtext = "";
                for (var key in JSON.parse(data.responseText)) {
                    errtext = JSON.parse(data.responseText)[key];
                }
                $("#snackbarerror").text(errtext);
                showerrtoast();

            }
        })
        .done(function(dataJson) {
            onloaddata(0);
            $("#snackbarsuccs").text("Profile Added Successfully!");
            showsuccesstoast();
            $('.addname').val('')
            $('.adddescription').val('')

        });
} //main function ends here

$('input').keypress(function() {
    $('input').removeClass("iserror");
});

//edit modal function
function editdescription(me) {
    localStorage.userid = me;
    $.ajax({
        url: retrivelist_api + localStorage.userid,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('#editmodalcontentdesc').modal();
            $('.editdescname').val(data.name)
            $('.editdesctextarea').val(data.description)
        },
        error: function(data) {
            console.log('error in loading retrival');
        }
    });
}

function editdescriptionimg(getval) {
    localStorage.userimgid = getval;
    $.ajax({
        url: retrivelist_api + localStorage.userimgid,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('#editmodalcontent').modal();
            $('.editnamemodal').val(data.name);
        },
        error: function(data) {
            console.log('error in loading retrival');
        }
    });
}

function updateimgmodal() {

    var editprofimagefile = $(".editimg")[0].files;

    if ($('.editnamemodal').val().trim() == '') {
        $('.editnamemodal').addClass("iserror");
        $("#snackbarerror").text("Name is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if (editprofimagefile.length == 0) {
        $("#snackbarerror").text("Image is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    var postData = new FormData();
    postData.append("name", $('.editnamemodal').val());
    postData.append("image", editprofimagefile[0]);

    $('.updateimgmodal').show();
    $.ajax({
            url: updateeditprofile_api + localStorage.userimgid + '/',
            type: 'put',
            data: postData,
            crossDomain: true,
            contentType: false,
            processData: false,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn);
            },
            success: function(data) {
                $('.updateimgmodal').hide();
                $('.updateimgmodal').attr("disabled", false);
                $(".closemodalimg").click();
            },
            error: function(data) {

                $('.updateimgmodal').hide();
                $('.updateimgmodal').attr("disabled", false);
                var errtext = "";
                for (var key in JSON.parse(data.responseText)) {
                    errtext = JSON.parse(data.responseText)[key];
                }
                $("#snackbarerror").text(errtext);
                showerrtoast();

            }
        })
        .done(function(dataJson) {
            onloaddata(0);
            $("#snackbarsuccs").text("Profile Updated Successfully!");
            showsuccesstoast();

        });
}


function updatemodal() {

    if ($('.editdescname').val().trim() == '') {
        $('.editdescname').addClass("iserror");
        $("#snackbarerror").text("Name is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('.editdesctextarea').val().trim() == '') {
        $('.editdesctextarea').addClass("iserror");
        $("#snackbarerror").text("Description is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    var postData = new FormData();
    postData.append("name", $('.editdescname').val());
    postData.append("description", $('.editdesctextarea').val());

    $('.updateimodal').show();
    $.ajax({
            url: updateeditprofile_api + localStorage.userid + '/',
            type: 'put',
            data: postData,
            crossDomain: true,
            contentType: false,
            processData: false,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn);
            },
            success: function(data) {
                $('.updateimodal').hide();
                $('.updateimodal').attr("disabled", false);
                $(".closeum").click();
            },
            error: function(data) {

                $('.updateimodal').hide();
                $('.updateimodal').attr("disabled", false);
                var errtext = "";
                for (var key in JSON.parse(data.responseText)) {
                    errtext = JSON.parse(data.responseText)[key];
                }
                $("#snackbarerror").text(errtext);
                showerrtoast();

            }
        })
        .done(function(dataJson) {
            onloaddata(0);
            $("#snackbarsuccs").text("Profile Updated Successfully!");
            showsuccesstoast();

        });
}


function deleteemployee(me) {
    localStorage.editid = me;
    $(".deletecard").text($(".restnameview" + me).text());
}

function deletecard() {
    $('.deletemodal').show();
    $.ajax({
        url: deleteprofile_api + localStorage.editid + '/',
        type: 'DELETE',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('.deletemodal').hide();
            $("#snackbarsuccs").text("Promotion has been deleted!");
            showsuccesstoast();
            onloaddata(0);
            $(".closeclassdelete").click();

        },
        error: function(data) {
            $('.deletemodal').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }) //done fn ends here

}

//load more function

function loadmore() {
    if (localStorage.nextpage != "") {
        onloaddata(1);
    }
}