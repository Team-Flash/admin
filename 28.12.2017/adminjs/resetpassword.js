$(function() {

    $(".resetpldr").hide();


    $(".ipclkrsp").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclkrsp").click();
        }
    });

    //validation fn starts here
    $("#password").keyup(function() {
        if ($("#password").val() == "") {
            $('#password').addClass("iserror");
            $("#snackbarerror").text("Password is Required");
            showerrtoast();
        }
    });


    $("#retype_password").keyup(function() {
        if ($("#retype_password").val() == "") {
            $('#retype_password').addClass("iserror");
            $("#snackbarerror").text("Re-enter password is Required");
            showerrtoast();
        }

    });

    //get query string fn starts here
    function getQueryStrings() {
        var assoc = {};
        var decode = function(s) {
            return decodeURIComponent(s.replace(/\+/g, " "));
        };
        var queryString = location.search.substring(1);
        var keyValues = queryString.split('&');
        for (var i in keyValues) {
            var key = keyValues[i].split('=');
            if (key.length > 1) {
                assoc[decode(key[0])] = decode(key[1]);
            }
        }
        return assoc;
    }
    var qs = getQueryStrings();
    var uid = qs["id"];
    var token = qs["token"];
    sessionStorage.resettoken = token;
    sessionStorage.resetuser_id = uid;


}); //initila fn endss here

$("input").keyup(function() {
    $(this).removeClass("iserror");
});

// reset password fn starts here
function rspcheck() {

    if ($('#password').val().trim() == '') {
        $('#password').addClass("iserror");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#password').val().length < 6) {
        $('#password').addClass("iserror");
        $("#snackbarerror").text("Min 6 Characters is Required for Password!");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#retype_password').val().trim() == '') {
        $('#retype_password').addClass("iserror");
        $("#snackbarerror").text("Re-enter password is required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#retype_password').val().length < 6) {
        $('#retype_password').addClass("iserror");
        $("#snackbarerror").text("Min 6 Characters is Required for Re-Enter Password!");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#retype_password').val() != $('#password').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showerrtoast();
        event.preventDefault();
        return;
    }


    $(".resetpldr").show();
    $(".rspbtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({
        "password": $('#password').val(),
        "uid": sessionStorage.getItem('resetuser_id'),
        "token": sessionStorage.getItem('resettoken'),
        "client": uniqueclient
    });

    $.ajax({
        url: resetpassword_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".resetpldr").hide();
            $(".rspbtn").attr("disabled", false);
        },
        error: function(data) {
            $(".resetpldr").hide();
            $(".rspbtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();
        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Your Password Has Been Set Successfully!");
        showsuccesstoast();

        localStorage.userdetails = JSON.stringify(dataJson);

        localStorage.userid = dataJson.id;
        localStorage.wufname = dataJson.first_name;
        localStorage.wulname = dataJson.last_name;
        localStorage.wutkn = dataJson.token;
        localStorage.wuemail = dataJson.email;
        localStorage.username = dataJson.username;

        setTimeout(function() {
            window.location.href = "dashboard.html";
        });

    });

} // reset password fn ends here