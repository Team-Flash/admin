$(function() { //initial fn starts here

    $(".changepwdldr").hide();

    //enter click fn signup/login starts here
    $(".ipclkcpwd").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclkcpwd").click();
        }!$(this).val() ? $(this).addClass("iserror") : $(this).removeClass("iserror");
    });

}); //initial fn ends here


//change password fn starts here
function changepassword() {

    if ($('#oldpwd').val().trim() == '') {
        $("#oldpwd").addClass("iserror");
        $("#snackbarerror").text("Old Password is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#oldpwd').val().length < 6) {
        $("#oldpwd").addClass("iserror");
        $("#snackbarerror").text("Atleast 6 characters is required for old password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#newpwd').val().trim() == '') {
        $("#newpwd").addClass("iserror");
        $("#snackbarerror").text("New Password is Required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#newpwd').val().length < 6) {
        $("#newpwd").addClass("iserror");
        $("#snackbarerror").text("Atleast 6 characters is required for New password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#oldpwd').val() == $('#newpwd').val()) {
        $("#snackbarerror").text("Old Password & New Password should not be same!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#renewpass').val().trim() == '') {
        $("#renewpass").addClass("iserror");
        $("#snackbarerror").text("Re - New Password is Required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#renewpass').val().length < 6) {
        $("#renewpass").addClass("iserror");
        $("#snackbarerror").text("Atleast 6 characters is required for re - new password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#newpwd').val() != $('#renewpass').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".changepwdldr").show();
    $(".changepassBtn").attr("disabled", true);

    var postdata = JSON.stringify({
        "current_password": $("#oldpwd").val(),
        "password": $("#newpwd").val()
    });

    $.ajax({
        url: changepassword_api,
        type: 'post',
        data: postdata,
        headers: {
            "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {

            $(".changepwdldr").hide();
            $(".changepassBtn").attr("disabled", false);

        },
        error: function(data) {

            $(".changepwdldr").hide();
            $(".changepassBtn").attr("disabled", false);

            var errtext = '';
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $(".closechangepass").click();

        $("#oldpwd,#newpwd,#renewpass").val("");

        $("#snackbarsuccs").text("Password Changed Sucessfully!");
        showsuccesstoast();

    });

} //change password fn ends here