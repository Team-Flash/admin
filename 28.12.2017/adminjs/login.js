$(function() {

    $(".lgnldr").hide();

    //enter click fn signup/login starts here
    $(".ipclklgn").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclklgn").click();
        }
    });


});

$("#loginemailid").keyup(function() {
    if ($("#loginemailid").val() == "") {
        $('#loginemailid').addClass("iserror");
    } else {
        $('#loginemailid').removeClass("iserror");
    }
});

$("#loginpasswrd").keyup(function() {
    if ($("#loginpasswrd").val() == "")
        $('#loginpasswrd').addClass("iserror");
    else
        $('#loginpasswrd').removeClass("iserror");

});

$(".inputfield").keyup(function() {
    $(this).removeClass("iserror");
});

//user login fn starts here
function userlogin() {

    if ($('#loginemailid').val().trim() == '') {
        $('#loginemailid').addClass("iserror");
        $("#snackbarerror").text("Email-Id is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    var x = $('#loginemailid').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#loginemailid').addClass("iserror");
        $("#snackbarerror").text("valid Email-Id is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#loginpasswrd').val().trim() == '') {
        $('#loginpasswrd').addClass("iserror");
        $("#snackbarerror").text("Password is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#loginpasswrd').val().length < 6) {
        $('#loginpasswrd').addClass("iserror");
        $("#snackbarerror").text("Atleast 6 character length password is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    $(".lgnldr").show();
    $(".lgnBtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({

        "username": $('#loginemailid').val(),
        "password": $('#loginpasswrd').val(),
        "client": uniqueclient,
        "role" : 1

    });

    $.ajax({
        url: login_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            $(".lgnldr").hide();
            $(".lgnBtn").attr("disabled", false);

        },
        error: function(data) {

            $(".lgnldr").hide();
            $(".lgnBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        localStorage.userdetails = JSON.stringify(dataJson);

        localStorage.userid = dataJson.id;
        localStorage.wufname = dataJson.first_name;
        localStorage.wulname = dataJson.last_name;
        localStorage.wutkn = dataJson.token;
        localStorage.wuemail = dataJson.email;
        localStorage.username = dataJson.username;
        localStorage.userrole = dataJson.userprofile.role.id;

        $("#snackbarsuccs").text("Login Success!");
        showsuccesstoast();

        //{
        //     "id": 1,
        //     "first_name": "",
        //     "last_name": "",
        //     "username": "krishna@billiontags.com",
        //     "email": "krishna@billiontags.com",
        //     "properties": [
        //         {
        //             "id": 1,
        //             "name": "Holiday Inn"
        //         }
        //     ],
        //     "token": "eb2c56ae9f59dbda903d66d96a2869f137d245d0"
        // }

        window.location.href = 'dashboard.html';

    }); //done fn ends here

} //login fn ends here